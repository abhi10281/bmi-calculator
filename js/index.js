function submit(){
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value; 
    var height= document.getElementById('height').value;
    var weight = document.getElementById('weight').value;

    
    function moveProgressBar (){
        var element = document.getElementById('submitprogress');
        var elementVal = element.value;
        var id = setInterval(frame, 10);
        function frame() {
            if (elementVal >= 100) {
            clearInterval(id);
            } else {
            elementVal++;
            element.value  =  elementVal;
            }
        }
        setTimeout(()=>{
            if(name && email &&  height &&  weight){
                const bmi =   weight / Math.pow(height , 2);
                document.getElementById('bmi-value').innerHTML ="BMI is:" +  bmi;
            }
            
        } ,1000)
    
    }

    moveProgressBar();
    
}